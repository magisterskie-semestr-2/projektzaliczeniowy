function getCart() {
    $.ajax({
        url: "http://localhost:8080/api/cart/items",
    }).done(function (data) {
        var suma = 0;
        data.forEach(function (item) {
            var row = document.createElement("tr");
            var col = document.createElement("td");
        
            var icon = document.createElement("img");
            icon.src = item.imgUrl;
            icon.width = 50;
            icon.height = 50;
            col.appendChild(icon);
            row.appendChild(col);
            col = document.createElement("td");
            col.innerText
                = item.name;
            row.appendChild(col);
            col = document.createElement("td");
            col.innerText
                = item.price;

            suma += item.price;
            row.appendChild(col);;

            col = document.createElement("td");
            btn = document.createElement("a");
            btn.innerText = "Usuń z koszyka";
            btn.classList.add("btn");
            btn.classList.add("btn-default")
            btn.addEventListener("click", function () {
                console.log(item.id);
                removeCart(item.id);
            });
            col.appendChild(btn);
            row.appendChild(col);

            document.getElementById("operators")
                .appendChild(row);
        });
        var s = document.getElementById("suma");
        s.innerText = "Łączna cena koszyka: " + suma + "zł";
    });
}

function removeCart(id) {
    $.ajax({
        url: "http://localhost:8080/api/cart/remove-item?id=" + id,
        method: "GET",
    }).done(function () {
        location.reload();
    })
}

jQuery(document).ready(function ($) {
    getCart();
    var submit_btn = document.getElementById("submit");
    submit_btn.addEventListener("click", function (event) {
        event.preventDefault();

        $.ajax({
            url: "http://localhost:8080/api/cart/clear",
            method: "GET",
        }).done(function () {
            location.reload();
        })
    });

    var submit_btn = document.getElementById("zamow");
    submit_btn.addEventListener("click", function (event) {
        event.preventDefault();

        $.ajax({
            url: "http://localhost:8080/api/orders/from-cart",
            method: "GET",
        }).done(function () {
            location.href="http://localhost:8080/orders"
        })
    });
});
