function getAllItems() {
    $.ajax({
        url: "http://localhost:8080/api/items/all",
    }).done(function (data) {
        data.forEach(function (item) {
            var row = document.createElement("tr");
            var col = document.createElement("td");
            var icon = document.createElement("img");
            icon.src = item.imgUrl;
            icon.width=50;
            icon.height=50;
            col.appendChild(icon);
            row.appendChild(col);
            col = document.createElement("td");
            col.innerText
                = item.name;
            row.appendChild(col);
            col = document.createElement("td");
            col.innerText
                = item.price;
            row.appendChild(col);
            col = document.createElement("td");
            col.innerText
                = item.amount;
            row.appendChild(col);

            col = document.createElement("td");
            btn = document.createElement("a");
            btn.innerText="Dodaj do koszyka";
            btn.classList.add("btn");
            btn.classList.add("btn-default")
            btn.addEventListener("click",function(){
                console.log(item.id);
                addCart(item.id);
            });
            col.appendChild(btn);
            row.appendChild(col);

            document.getElementById("items")
                .appendChild(row);
        });
    });
}

function addCart(id){
    $.ajax({
        url: "http://localhost:8080/api/cart/add-item?id="+id,
        method:"GET",
    }).done(function () {
        location.href = "http://localhost:8080/cart"
    })
}

jQuery(document).ready(function($) {
    getAllItems();
});
