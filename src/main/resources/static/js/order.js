function getCart() {
    var id = getParameterByName('id');
    $.ajax({
        url: "http://localhost:8080/api/orders/items?id=" + id,
    }).done(function (data) {
        var suma = 0;
        data.forEach(function (item) {
            var row = document.createElement("tr");
            var col = document.createElement("td");

            var icon = document.createElement("img");

            icon.src = item.item.imgUrl;
            icon.width = 50;
            icon.height = 50;
            col.appendChild(icon);
            row.appendChild(col);
            col = document.createElement("td");
            col.innerText
                = item.item.name;
            row.appendChild(col);
            col = document.createElement("td");
            col.innerText
                = item.price;
            suma += item.price;

            row.appendChild(col);;

            document.getElementById("operators")
                .appendChild(row);
        });
        var s = document.getElementById("suma");
        s.innerText = "Łączna cena koszyka: " + suma + "zł";
    });
}


jQuery(document).ready(function ($) {
    getCart();
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}