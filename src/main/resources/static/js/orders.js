function getOrders() {
    $.ajax({
        url: "http://localhost:8080/api/orders/all",
    }).done(function (data) {
        data.forEach(function (item) {
            var row = document.createElement("tr");
            var col = document.createElement("td");
            col = document.createElement("td");
            col.innerText
                = item.id;
            row.appendChild(col);
            col = document.createElement("td");
            col.innerText
                = item.price;
            row.appendChild(col);;

            col = document.createElement("td");
            btn = document.createElement("a");
            btn.innerText = "Zobacz zamówienie";
            btn.classList.add("btn");
            btn.classList.add("btn-default")
            btn.addEventListener("click", function () {
                location.href="http://localhost:8080/order?id="+item.id
            });
            col.appendChild(btn);
            row.appendChild(col);

            document.getElementById("operators")
                .appendChild(row);
        });
    });
}


jQuery(document).ready(function ($) {
    getOrders();
});

