package com.example.demo.ApiControllers;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.demo.DataService.*;
import com.example.demo.Models.*;
import java.util.List;

@RestController
@RequestMapping("/api/items")
public class ItemsController{
    @Autowired
    private IShopService service;

    @GetMapping(value = "/all", produces="application/json")
    public List<ItemModel> getAll(){
        return service.getAllItems();
    }

    @GetMapping(value = "/one", produces = "application/json")
    public ItemModel getById(@RequestParam("id") int id){
        return service.getItemById(id);
    }

    @GetMapping(value = "/multi", produces = "application/json")
    public Iterable<ItemModel> getByIds(@RequestParam("ids") List<Integer> ids){
        return service.getItemsByIds(ids);
    }
}