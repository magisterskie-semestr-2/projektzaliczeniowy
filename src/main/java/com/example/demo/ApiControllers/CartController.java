package com.example.demo.ApiControllers;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import com.example.demo.DataService.*;

@RestController
@RequestMapping("/api/cart")
public class CartController {
    @Autowired
    private IShopService service;

    @GetMapping(value = "/items", produces = "application/json")
    public Object getCartItems(Authentication authentication) {
        return service.getCartItems(authentication.getName());
    }

    @RequestMapping(value = "/add-item",  method = RequestMethod.GET)
    public void addItemToCart(Authentication authentication, int id) {
        service.addItemToCart(authentication.getName(), id);
    }

    @RequestMapping(value = "/remove-item",  method = RequestMethod.GET)
    public void removeItemFromCart(Authentication authentication, int id) {
        service.removeItemFromCart(authentication.getName(), id);
    }

    @RequestMapping(value = "/clear",  method = RequestMethod.GET)
    public void ClearCart(Authentication authentication) {
        service.clearCart(authentication.getName());
    }
}
