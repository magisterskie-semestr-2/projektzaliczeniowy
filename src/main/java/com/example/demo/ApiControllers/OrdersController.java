package com.example.demo.ApiControllers;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import com.example.demo.DataService.*;

@RestController
@RequestMapping("/api/orders")
public class OrdersController {
    @Autowired
    private IShopService service;

    @GetMapping(value = "/all", produces = "application/json")
    public Object getAll(Authentication authentication) {
        return service.getOrdersByUsername(authentication.getName());
    }

    @GetMapping(value = "/items", produces = "application/json")
    public Object getOrderItems(Authentication authentication, @RequestParam("id") int id) {
        return service.getOrderItems(authentication.getName(), id);
    }

    @GetMapping(value = "/from-cart", produces = "application/json")
    public void getOrderItems(Authentication authentication) {
        service.cartToOrder(authentication.getName());
    }
}
