package com.example.demo.Repositories;

import org.springframework.data.repository.CrudRepository;
import com.example.demo.Models.*;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends CrudRepository<CartModel, Integer>  {
    CartModel findByUsername(String username);
    CartModel findById(int id);
}
