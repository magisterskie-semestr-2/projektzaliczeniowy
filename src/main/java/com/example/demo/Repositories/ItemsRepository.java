package com.example.demo.Repositories;

import org.springframework.data.repository.CrudRepository;
import com.example.demo.Models.*;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ItemsRepository extends CrudRepository<ItemModel, Integer> {
    List<ItemModel> findAll();
    ItemModel findById(int id);
}
