package com.example.demo.Repositories;

import org.springframework.data.repository.CrudRepository;
import com.example.demo.Models.*;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface OrdersRepository extends CrudRepository<OrderModel, Integer> {
    List<OrderModel> findByUsername(String username);
    OrderModel findById(int id);
}
