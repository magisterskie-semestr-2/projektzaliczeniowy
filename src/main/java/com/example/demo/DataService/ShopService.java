package com.example.demo.DataService;

import org.springframework.stereotype.Service;
import com.example.demo.Models.*;
import com.example.demo.Repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

import java.util.*;

@Service
public class ShopService implements IShopService {

    private ItemsRepository itemsRepo;

    private OrdersRepository ordersRepo;

    private CartRepository cartRepo;

    @Autowired
    public ShopService(CartRepository cartRepo, OrdersRepository ordersRepo, ItemsRepository itemsRepo) {
        this.cartRepo = cartRepo;
        this.ordersRepo = ordersRepo;
        this.itemsRepo = itemsRepo;
    }

    public List<ItemModel> getAllItems() {
        return itemsRepo.findAll();
    }

    public ItemModel getItemById(int id) {
        return itemsRepo.findById(id);
    }

    public Iterable<ItemModel> getItemsByIds(Iterable<Integer> ids) {
        return itemsRepo.findAllById(ids);
    }

    public List<OrderModel> getOrdersByUsername(String username) {
        return ordersRepo.findByUsername(username);
    }

    public Set<OrderItemModel> getOrderItems(String username, int orderId) {
        OrderModel order = ordersRepo.findById(orderId);
        if (order == null || !order.getUsername().equals(username)) {
            return null;
        }

        return order.getOrderItem();
    }

    public void cartToOrder(String username) {
        CartModel cart = cartRepo.findByUsername(username);
        if (cart == null || cart.getCartItems().isEmpty()) {
            return;
        }

        OrderModel order = new OrderModel();
        Set<OrderItemModel> orderItems = new HashSet<OrderItemModel>();
        double sum = 0;
        for (CartItemModel item : cart.getCartItems()) {
            OrderItemModel orderItem = new OrderItemModel();
            orderItem.setItem(item.getItem());
            orderItem.setOrder(order);
            double price = item.getItem().getPrice();
            orderItem.setPrice(price);
            sum += price;

            orderItems.add(orderItem);
        }
        order.setUsername(username);
        order.setPrice(sum);
        order.setOrderItem(orderItems);

        ordersRepo.save(order);

        clearCart(username);
    }

    @Transactional
    private CartModel createOrGetCart(String username) {
        CartModel cart = cartRepo.findByUsername(username);
        if (cart != null) {
            return cart;
        }
        cart = new CartModel();
        cart.setUsername(username);
        cart.setCartItems(new HashSet<CartItemModel>());

        cartRepo.save(cart);
        return cart;
    }

    public List<ItemModel> getCartItems(String username) {
        CartModel cart = createOrGetCart(username);
        List<ItemModel> items = new ArrayList<ItemModel>();
        for (CartItemModel item : cart.getCartItems()) {
            items.add(item.getItem());
        }

        return items;
    }

    // @Transactional
    public void addItemToCart(String username, int id) {
        CartModel cart = createOrGetCart(username);
        Set<CartItemModel> cartItems = cart.getCartItems();
        ItemModel item = itemsRepo.findById(id);
        CartItemModel cartItem = new CartItemModel();
        cartItem.setCart(cart);
        cartItem.setItem(item);

        cartItems.add(cartItem);
        cart.setCartItems(cartItems);

        cartRepo.save(cart);
    }

    @Transactional
    public void removeItemFromCart(String username, int id) {
        CartModel cart = createOrGetCart(username);
        Set<CartItemModel> cartItems = cart.getCartItems();

        for (CartItemModel item : cartItems) {
            if (item.getItem().getId() == id) {
                cartItems.remove(item);
                break;
            }
        }
        cart.setCartItems(cartItems);
        cartRepo.save(cart);
    }

    @Transactional
    public void clearCart(String username) {
        CartModel cart = createOrGetCart(username);
        Set<CartItemModel> cartItems = cart.getCartItems();
        cartItems.clear();
        cart.setCartItems(cartItems);
        cartRepo.save(cart);
    }

    @EventListener
    @Transactional
    public void Seed(ContextRefreshedEvent event) {
        ItemModel item1 = new ItemModel();
        item1.setName("Procesor AMD Ryzen 9 5950X, 3.4GHz, 64 MB, BOX (100-100000059WOF)");
        item1.setImgUrl("https://images.morele.net/i1064/7267020_0_i1064.jpg");
        item1.setPrice(3999);
        item1.setAmount(30);

        ItemModel item2 = new ItemModel();
        item2.setName("Karta graficzna Gigabyte GeForce RTX 3070 Gaming OC 8GB GDDR6 (GV-N3070GAMING OC-8GD)");
        item2.setImgUrl("https://images.morele.net/i1064/5944660_0_i1064.jpg");
        item2.setPrice(6199);
        item2.setAmount(30);

        itemsRepo.save(item1);
        itemsRepo.save(item2);
    }
}