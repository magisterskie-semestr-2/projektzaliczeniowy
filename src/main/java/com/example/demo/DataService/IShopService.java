package com.example.demo.DataService;

import com.example.demo.Models.*;
import java.util.List;
import java.util.Set;

public interface IShopService {
    //ITEMS
    List<ItemModel> getAllItems();
    ItemModel getItemById(int id);
    Iterable<ItemModel> getItemsByIds(Iterable<Integer> ids);
    //ORDERS
    List<OrderModel> getOrdersByUsername(String username);
    Set<OrderItemModel> getOrderItems(String username, int orderId);
    void cartToOrder(String username);
    //CART
    List<ItemModel> getCartItems(String username);
    void addItemToCart(String username, int id);
    void removeItemFromCart(String username, int id);
    void clearCart(String username);
}