package com.example.demo.Models;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class OrderItemModel {
    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "OrderItemId")
    private int id; 

    @JsonIgnore
    @OneToOne
    @JoinColumn(name="OrderId")
    private OrderModel order;

    @OneToOne
    @JoinColumn(name="ItemId")
    private ItemModel item;

    @Column(name = "Price")
    private double price;


    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public OrderModel getOrder() {
        return this.order;
    }

    public void setOrder(OrderModel order) {
        this.order = order;
    }

    public ItemModel getItem() {
        return this.item;
    }

    public void setItem(ItemModel item) {
        this.item = item;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
