package com.example.demo.Models;

import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class OrderModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "OrderId")
    private int id;

    @Column(name = "Username")
    private String username;

    @Column(name = "Price")
    private double price;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<OrderItemModel> orderItem;


    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Set<OrderItemModel> getOrderItem() {
        return this.orderItem;
    }

    public void setOrderItem(Set<OrderItemModel> orderItem) {
        this.orderItem = orderItem;
    }
}
