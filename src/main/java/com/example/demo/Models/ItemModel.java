package com.example.demo.Models;

// import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class ItemModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ItemId")
    private int id;

    @Column(name = "Name")
    private String name;

    @Column(name = "ImageUrl")
    private String imgUrl;

    @Column(name = "Price")
    private double price;

    @Column(name = "Amount")
    private int amount;

    public int getId() {
        return this.id;
    }

    public void setItemId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return this.imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    // private ItemGroupModel group;
}