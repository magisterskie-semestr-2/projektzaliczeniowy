package com.example.demo.Models;

import javax.persistence.*;

@Entity
public class CartItemModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CartItemId")
    private int id; 

    @OneToOne
    @JoinColumn(name="CartId")
    private CartModel cart;

    @OneToOne
    @JoinColumn(name="ItemId")
    private ItemModel item;


    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CartModel getCart() {
        return this.cart;
    }

    public void setCart(CartModel cart) {
        this.cart = cart;
    }

    public ItemModel getItem() {
        return this.item;
    }

    public void setItem(ItemModel item) {
        this.item = item;
    }
}
