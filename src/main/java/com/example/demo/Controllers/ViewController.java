package com.example.demo.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;


@Controller
public class ViewController {
    @RequestMapping("/")
    public RedirectView home(RedirectAttributes attributes){
        return new RedirectView("items/all");
    }

    @RequestMapping("/items/all")
    public String allItems(){
        return "allItems"; 
    }

    @RequestMapping("/cart")
    public String cart(){
        return "cart"; 
    }

    @RequestMapping("/orders")
    public String orders(){
        return "orders"; 
    }

    @RequestMapping("/order")
    public String order(){
        return "order"; 
    }
}
